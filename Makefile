.PHONY: install_and_lock

install_and_lock:
	pip install -r requirements/directly-needed.txt
	pip freeze > requirements/locked.txt

run_server:
	python main.py start
	#uvicorn app:app --reload

create_secret:
	python main.py create-secret
#	PYTHONPATH=$(pwd)/app

create_db:
	python main.py create-db

create_admin:
	python main.py create-admin admin random-password