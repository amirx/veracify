# Abstract

It is very difficult to separate facts from fiction on the internet. We have tried many forms of "fact checking" approaches, but these have an inherent fault. If we could tell which authority to trust on the internet, we would not be in this situation.

Our proposal is to allow shared note taking on the internet. Each user will specify a number of other users as trusted. These connections will form a network where mutual trust between users can be computed. 

Each user will also be able to fact check and provide insights into a particular subject. When another user needs to obtain related information he will be provided with the recorded information according to his own network.

We propose to try different applications on top of these two mechanism to see which one sticks. This includes product reviews, cold calling rejection, fake profile recognition and News website verification.

# Introduction

When the internet was created 30 years ago humanity envisioned the idea that everyone can publish information and have access to such information. This came true. What we did not envision was drowning by the volume of this information. The internet is currently filled with misinformation much more than information. Indeed it's much easier to create such misinformation because there is little checks and bounds. Furthermore misinformation travels much quicker than correct information because correct information is mostly boring and raises very little emotions.

Although many efforts are underway to verify the integrity of articles and stories on the internet, we are far from an ideal situation. We postulate that there are 2 reasons for this. The first is that people don't trust 3rd party fact checkers. The second reason is that 3rd party fact checkers can't scale with the number of internet users, where as, falsehoods do.

This is similar to the ssl certificate problem where we need to have a "trusted authority" to ultimately verify the integrity of a certificate. Unfortunately it is unlikely for humanity to converge on a single "trusted authority". Hence we propose users should designate their own trusted network. We proposit that this would alliviate much of the trust issues that we have on the internet and allow us to quickly find what can be trusted and what should be dissmissed.

In our platform each user will designate other friends as trusted. My vision is that we only designate people that we have regular physical contact with and that we can trust. People who we can call up and ask a question from whenever we want. Then the platform would act like an async and transitive implementation of the same mechanism. That is you get the opinion from a friend of a friend by asking a question at some point in time and them giving an answer at some other point in time, before or after the question. 

The implementation of such a system could have some difficulties and we will provide provide several approaches in the next section. The section after will be dedicated to possible applications.

# Technical details

# Possible applications
