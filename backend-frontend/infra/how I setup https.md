used google domains to point veracify.amirhb.com to the ip of the server

installed nginx (apt install nginx)

changed this file /etc/nginx/sites-available/default by adding the following:
location /route {
    proxy_pass  http://127.0.0.1:8000/;
}

followed this to setup the ssh:
https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal

1. sudo snap install --classic certbot
1. sudo ln -s /snap/bin/certbot /usr/bin/certbot
1. sudo certbot --nginx