timestamp=$(date +%s)

deployfilename=deploy$timestamp.tar

tar -cf tmp/$deployfilename ./app ./alembic ./alembic.ini ./requirements-locked.txt docker-compose.yml .env Makefile alembic.ini
scp tmp/$deployfilename root@165.22.80.9:/root/$deployfilename

cat << EOF | ssh root@165.22.80.9
apt install python3.11-venv python3-pip nginx docker-compose -y
cd / && rm deploy -rf && mkdir deploy && tar -xvf /root/$deployfilename -C /deploy/
cd /deploy
docker-compose up -d
python3 -m venv venv
export PATH=/deploy/venv/bin:$PATH
/deploy/venv/bin/pip install -r requirements-locked.txt &&
docker exec postgres psql -U postgres fastapi -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'
make push-migration
pkill -9 uvicorn
nohup uvicorn app.main:app --port 8000 >> /logs-$timestamp.log 2>&1 &
EOF