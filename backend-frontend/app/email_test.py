# from fastapi_mail import FastMail
import asyncio
from unittest import TestCase, mock

from app.email import Email
from app.models import User
import pytest


@pytest.fixture
def loop():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


class AsyncMock(mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)

def test_when_not_verified_email_is_sent(loop: asyncio.AbstractEventLoop):
    user = mock.MagicMock(User)
    user.verified = False
    fm = AsyncMock()
    mail = Email(fm)
    db = mock.MagicMock()
    user_query = mock.MagicMock()

    r = mail.send_verification_email(db, user, ["recipient@recipient.com"], "http://somehost:port", user_query)
    loop.run_until_complete(r)

    fm.send_message.assert_called()


def test_when_verified_no_email_is_sent(loop: asyncio.AbstractEventLoop):
    user = mock.MagicMock(User)
    user.verified = True
    fm = AsyncMock()
    mail = Email(fm)
    db = mock.MagicMock()
    user_query = mock.MagicMock()

    r = mail.send_verification_email(db, user, ["recipient@recipient.com"], "http://somehost:port", user_query)
    loop.run_until_complete(r)

    fm.send_message.assert_not_called()