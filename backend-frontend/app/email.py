import hashlib
from random import randbytes
from typing import List

from fastapi import HTTPException
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
from pydantic import EmailStr, BaseModel
from starlette import status

from jinja2 import Environment, select_autoescape, PackageLoader

from app.models import User

env = Environment(
    loader=PackageLoader('app', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


class EmailSchema(BaseModel):
    email: List[EmailStr]


class Email:
    def __init__(self, fastMail: FastMail):
        self.sender = 'Codevo <admin@admin.com>'
        self.fm = fastMail

    @staticmethod
    def make_from_env_config(settings):
        # Define the config
        conf = ConnectionConfig(
            MAIL_USERNAME=settings.EMAIL_USERNAME,
            MAIL_PASSWORD=settings.EMAIL_PASSWORD,
            MAIL_FROM=settings.EMAIL_FROM,
            MAIL_PORT=settings.EMAIL_PORT,
            MAIL_SERVER=settings.EMAIL_HOST,
            MAIL_STARTTLS=False,
            MAIL_SSL_TLS=False,
            USE_CREDENTIALS=True,
            VALIDATE_CERTS=True
        )
        return FastMail(conf)

    async def sendMail(self, user: User, url: str, recipients: List[EmailStr], subject, template):

        # Generate the HTML template base on the template name
        template = env.get_template(f'{template}.html')

        html = template.render(
            url=url,
            first_name=user.name,
            subject=subject
        )

        # Define the message options
        message = MessageSchema(
            subject=subject,
            recipients=recipients,
            body=html,
            subtype="html"
        )

        # Send the email
        await self.fm.send_message(message)

    async def sendVerificationCode(self, user: User, url: str, email):
        await self.sendMail(user, url, email, 'Your verification code (Valid for 10min)', 'verification')

    def get_initial_url_part(self, settings, request):
        if settings.VERIFICATION_DOMAIN:
            return settings.VERIFICATION_DOMAIN
        return f"{request.url.scheme}://{request.client.host}:{request.url.port}"
    async def send_verification_email(self, db, user: User, email, initial_url_part, user_query):
        try:
            # Send Verification Email
            if not user.verified:
                token = randbytes(10)
                hashedCode = hashlib.sha256()
                hashedCode.update(token)
                verification_code = hashedCode.hexdigest()
                user_query.update({'verification_code': verification_code}, synchronize_session=False)
                db.commit()
                url = f"{initial_url_part}/api/auth/verifyemail/{token.hex()}"
                await self.sendVerificationCode(user, url, email)
        except Exception as error:
            print('Error', error)
            user_query.update(
                {'verification_code': None}, synchronize_session=False)
            db.commit()
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                detail='There was an error sending email')
        return {'status': 'success', 'message': 'Verification token successfully sent to your email'}
